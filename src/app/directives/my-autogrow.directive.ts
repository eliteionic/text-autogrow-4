import { Directive, Renderer2 } from '@angular/core';
import { DomController } from '@ionic/angular';

@Directive({
  	selector: '[my-autogrow]',
	host: {
		'(input)' : 'resize($event.target)'
	}
})
export class MyAutogrowDirective {

	constructor(private renderer: Renderer2, private domCtrl: DomController) {

	}

	resize(textarea) {

		let newHeight;

		this.domCtrl.read(() => {
			newHeight = textarea.scrollHeight;
		});

		this.domCtrl.write(() => {
			this.renderer.setStyle(textarea, 'height', newHeight + 'px');
		});

	}

}